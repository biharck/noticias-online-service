var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ImagesSchema   = new Schema({
    url: String
});

module.exports = mongoose.model('Images', ImagesSchema);