var mysql = require('mysql');

var db;

function connectDatabase() {
    if (!db) {
        db = mysql.createConnection({
            host: "localhost",
            user: "noticias_online",
            password: "noticias_online_ganymede",
            database: "noticias_online"
        });

        // db = mysql.createConnection({
        //     host: "10.211.55.3",
        //     user: "root",
        //     password: "ganymede",
        //     database: "noticias_online"
        // });

        // db = mysql.createConnection({
        //     host: "137.74.160.108",
        //     user: "root",
        //     password: "ganymede",
        //     database: "noticias_online"
        // });

        db.connect(function(err){
            if(!err) {
                console.log('Database is connected!');
            } else {
                console.log('Error connecting database!');
            }
        });
    }
    return db;
}

module.exports = connectDatabase();