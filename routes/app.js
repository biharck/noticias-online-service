var express = require('express');
var router = express.Router();
var Article = require('../models/article');
var passwordHash = require('password-hash');
var token = 'Jkl272opiut25';
var db = require('../db');


//token = sha1$25abf7a7$1$4a9b463e39de32465fc02d52804d9bfe74816c56

// all routes are protect bellow
// session token validation
// router.use('/', function (req, res, next) {

//     if(!passwordHash.verify(token, req.query['t'])){
//         return res.status(404).json({
//             title: 'Authentication Failed',
//             error: {message: 'You are not welcome here!'}
//         });
//     }
//     next();
// });


function cleanup(arr, prop) {
    var new_arr = [];
    var lookup  = {};
 
    for (var i in arr) {
        lookup[arr[i][prop]] = arr[i];
    }
 
    for (i in lookup) {
        new_arr.push(lookup[i]);
    }
 
    return new_arr;
}


router.get('/', function (req, res, next) {

    var category = req.query['category'];

    //datas mais novas
    var gt = req.query['gt'];

    //datas mais antigas
    var lt = req.query['lt'];

    var query = 'SELECT a.id as _id, a.title, a.pubdate, a.thumbnail, a.meta_title, a.meta_logo, a.link FROM article a JOIN category c on a.id = c.id_article WHERE 1 = 1 ';
    if(category){
        query = query + " and c.name = '"+category+"'";
    }
    
    if(gt){
       
        var a = new Date(gt);        
        var gt_date = a.getFullYear() + '-'+ (a.getMonth()+1) + '-' + a.getDate();
        gt_date = gt_date +' ' + a.getHours() + ':' + a.getMinutes() + ':' + a.getSeconds();
       
        query = query + " and a.pubdate > '" + gt_date + "'";
        // console.log(query);
    }
    if(lt){
        var a = new Date(lt);        
        var lt_date = a.getFullYear() + '-'+ (a.getMonth()+1) + '-' + a.getDate();
        lt_date = lt_date +' ' + a.getHours() + ':' + a.getMinutes() + ':' + a.getSeconds();
        query = query + " and a.pubdate < '" + lt_date + "'";
    }

    query = query + ' and pubdate < now() and a.content is not null group by a.id order by pubdate desc LIMIT 15';

    // console.log(query);
    db.query(query, function(err, rows) {
        if(err){
            console.log(err);
        }
        return res.status(200).json({
            message: 'Success',
            obj: rows
        });
    });
      
});

router.get('/:id', function (req, res, next) {

    var query = 'SELECT a.id as _id, a.meta_logo, a.title, a.pubdate, a.content, a.meta_title, a.link FROM article a WHERE a.id = ? ';

    
    db.query(query, req.params.id, function(err, rows) {
        res.status(200).json({
            message: 'Success',
            obj: rows
        });
    });

});

router.post('/device_register', function (req, res, next) {


    if(!req.body.token || !req.body.platform){
        return res.status(500).json({
                message: 'Failure'
            });
    }

    var query = "SELECT id from device where token = ? ";

    var device = {
        token: req.body.token,
        platform: req.body.platform,
        dt_register: new Date()
    };
    
    db.query(query, req.body.token, function(err, rows) {
        if(err) throw err;
        if(rows.length > 0){ 
            // return res.status(200).json({
            //         message: 'Success, token already existis'
            //     });
        }else{
            db.query('INSERT INTO device SET ?', device, function(err_c,res_c){
              if(err_c) throw err_c;    
                // res.status(200).json({
                //     message: 'Success'
                // });
            }); 
        }
    });

    res.status(200).json({
        message: 'Success'
    });

});

module.exports = router;
