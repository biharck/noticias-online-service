var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var Images       = require('./images');

var ArticleSchema   = new Schema({
    guid: String,
    title: String,
    description: String,
    category: String,
    author: String,
    thumbnail: String,
    pubdate: Date,
    link: String,
    meta_title: String,
    meta_logo: String,
    meta_link: String,
    images: [Images]
});

module.exports = mongoose.model('Article', ArticleSchema);